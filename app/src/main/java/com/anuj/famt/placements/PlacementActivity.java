package com.anuj.famt.placements;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.anuj.famt.BaseActivity;
import com.anuj.famt.R;
import com.anuj.famt.adapters.SliderAdapter;
import com.anuj.famt.utils.Constants;
import com.anuj.famt.utils.WebViewActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by playtm on 24/03/18.
 */

public class PlacementActivity extends BaseActivity {

    private ViewPager vpSlider;
    private LinearLayout noInternet;
    private ProgressBar progressBar;
    private List<String> list = new ArrayList<>();
    private Button readMore;

    @Override
    protected int getParentView() {
        return R.layout.activity_placement;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTitle("Placements");
        setup();
    }

    private void setup() {
        vpSlider = findViewById(R.id.viewpager);
        progressBar = findViewById(R.id.progress);
        progressBar.setVisibility(View.VISIBLE);
        noInternet = findViewById(R.id.ll_ni);
        readMore = findViewById(R.id.read);
        callWS();
        readMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PlacementActivity.this, WebViewActivity.class);
                intent.putExtra("link", "http://pwaman.x10host.com/famttnp/placement-record/");
                intent.putExtra("title", "Placement Details");
                startActivity(intent);
            }
        });

    }

    private void callWS() {

        Constants.FIREBASE_REF.child(Constants.PLACEMENT).child(Constants.PLACEMENT_IMAGES).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    list.add(snapshot.getValue().toString());
                }
                CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);


                vpSlider.setAdapter(new SliderAdapter(PlacementActivity.this, list));
                indicator.setViewPager(vpSlider);
                progressBar.setVisibility(View.GONE);

                /*if (dataSnapshot != null) {
                    HomeModel model = new HomeModel();
                    model.isHeaderImage = true;
                    listHome.add(model);

                    model = new HomeModel();
                    model.isHeader = true;
                    model.header = "News";
                    listHome.add(model);

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        listHome.add(snapshot.getValue(HomeModel.class));
                    }

                    getEvents();


                }*/

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                noInternet.setVisibility(View.VISIBLE);
            }
        });
    }

}
