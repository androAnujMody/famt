package com.anuj.famt;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.anuj.famt.home.Details;
import com.anuj.famt.models.HomeModel;
import com.anuj.famt.notices.NoticeActivity;
import com.anuj.famt.utils.Constants;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

/**
 * Created by anuj on 24/03/18.
 */

public class MyService extends Service {

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    @Override
    public void onCreate() {
        super.onCreate();
        pref = getSharedPreferences("FAMT", 0);
        editor = pref.edit();
        Log.e("", "service started");
        test();
    }

    public void test() {

        Constants.FIREBASE_REF.child(Constants.EVENTS).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (pref.getBoolean("added", false)) {
                    HomeModel homeModel = dataSnapshot.getValue(HomeModel.class);
                    showNoti("events", homeModel.detail, homeModel.detail, homeModel.heading);

                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        Constants.FIREBASE_REF.child(Constants.NEWS).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (pref.getBoolean("added", false)) {
                    HomeModel homeModel = dataSnapshot.getValue(HomeModel.class);
                    showNoti("news", homeModel.detail, homeModel.heading, homeModel.img);

                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        Constants.FIREBASE_REF.child(Constants.NOTICES).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (pref.getBoolean("added", false)) {
                    showNoti("notice", "", "", "");
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void showNoti(String from, String image, String detail, String heading) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationId = 1;
        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.noti_icon);

        if (from == "events") {
            mBuilder.setContentTitle("New Event Added");
            mBuilder.setContentText(heading);
            Intent intent = new Intent(this, Details.class);
            intent.putExtra(Constants.HEADER, heading);
            intent.putExtra(Constants.DETAILS, detail);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(pendingIntent);
        } else if (from == "news") {
            mBuilder.setContentTitle("New News Added");
            mBuilder.setContentText(heading);
            Intent intent = new Intent(this, Details.class);
            intent.putExtra(Constants.HEADER, heading);
            intent.putExtra(Constants.IMAGE, image);
            intent.putExtra(Constants.DETAILS, detail);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(pendingIntent);
        } else if (from == "notice"){
            mBuilder.setContentTitle("New Notice Added");
            mBuilder.setContentText(heading);
            Intent intent = new Intent(this, NoticeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(pendingIntent);
        }

        /*TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        mBuilder.setContentIntent(resultPendingIntent);*/

        mBuilder.setAutoCancel(true);
        notificationManager.notify(notificationId, mBuilder.build());

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startid) {
        return START_STICKY;
    }
}
