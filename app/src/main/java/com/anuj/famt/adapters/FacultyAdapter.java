package com.anuj.famt.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anuj.famt.R;
import com.anuj.famt.models.Faculties;
import com.bumptech.glide.Glide;

import java.util.List;


/**
 * Created by playtm on 17/02/18.
 */

public class FacultyAdapter extends RecyclerView.Adapter<FacultyAdapter.FacultyVH> {

    private Context context;
    private List<Faculties> listFaculty;
    private LayoutInflater inflater;

    public FacultyAdapter(Context context, List<Faculties> listFaculty) {
        this.context = context;
        this.listFaculty = listFaculty;
        inflater = LayoutInflater.from(context);
    }

    @Override

    public FacultyVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FacultyVH(inflater.inflate(R.layout.single_faculty_item, parent, false));
    }

    @Override
    public void onBindViewHolder(FacultyVH holder, int position) {
        holder.tvName.setText(listFaculty.get(position).facultyName + " (" + listFaculty.get(position).qualification + ")");
        holder.tvDesignation.setText(listFaculty.get(position).designation);
        if (!TextUtils.isEmpty(listFaculty.get(position).facultyImg)){
            Glide.with(context).load(listFaculty.get(position).facultyImg).into(holder.imageView);

        }
    }

    @Override
    public int getItemCount() {
        return listFaculty.size();
    }

    public class FacultyVH extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView tvName, tvDesignation;

        public FacultyVH(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_fac);
            tvName = itemView.findViewById(R.id.tv_fac_name);
            tvDesignation = itemView.findViewById(R.id.tv_fac_des);
        }
    }
}
