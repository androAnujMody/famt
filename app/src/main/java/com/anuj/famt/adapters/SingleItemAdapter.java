package com.anuj.famt.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anuj.famt.R;
import com.anuj.famt.departments.DepartmentDetails;
import com.anuj.famt.models.SingleModel;
import com.anuj.famt.notices.NoticeActivity;
import com.anuj.famt.utils.Constants;
import com.anuj.famt.utils.WebViewActivity;

import java.util.List;

/**
 * Created by anuj on 27/10/17.
 */

public class SingleItemAdapter extends RecyclerView.Adapter<SingleItemAdapter.SingleItemVH> {


    private Context context;
    private List<SingleModel> listData;
    private LayoutInflater inflater;
    private String from;

    public SingleItemAdapter(Context context, List<SingleModel> listData, String from) {
        this.context = context;
        this.listData = listData;
        this.from = from;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public SingleItemAdapter.SingleItemVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SingleItemVH(inflater.inflate(R.layout.single_department_item, parent, false));
    }

    @Override
    public void onBindViewHolder(SingleItemAdapter.SingleItemVH holder, int position) {
        holder.tvName.setText(listData.get(position).name);
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class SingleItemVH extends RecyclerView.ViewHolder {

        TextView tvName;

        public SingleItemVH(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.textView8);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (from.equalsIgnoreCase(Constants.DEPT)) {

                        Intent intent = new Intent(context, DepartmentDetails.class);
                        if (getLayoutPosition() == 0) {
                            intent.putExtra(Constants.DEPT, "Mechanical");
                        }

                        if (getLayoutPosition() == 1) {
                            intent.putExtra(Constants.DEPT, "Chemical");
                        }

                        if (getLayoutPosition() == 2) {
                            intent.putExtra(Constants.DEPT, "Electrical");
                        }

                        if (getLayoutPosition() == 3) {
                            intent.putExtra(Constants.DEPT, "EXTC");
                        }

                        if (getLayoutPosition() == 4) {
                            intent.putExtra(Constants.DEPT, "IT");
                        }

                        context.startActivity(intent);

                    }


                    if (from.equalsIgnoreCase(Constants.TT)) {
                        Intent intent = new Intent(context, WebViewActivity.class);
                        intent.putExtra("title", "Time Table");
                        intent.putExtra("link", "https://docs.google.com/viewer?url=" + listData.get(getLayoutPosition()).link);
                        context.startActivity(intent);

                    }

                    if (from.equalsIgnoreCase(Constants.STUDENT)) {
                        Intent intent = new Intent(context, NoticeActivity.class);
                        context.startActivity(intent);

                    }
                }
            });
        }
    }
}
