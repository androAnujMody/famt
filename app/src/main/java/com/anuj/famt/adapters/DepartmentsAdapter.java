package com.anuj.famt.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by playtm on 12/02/18.
 */

public class DepartmentsAdapter extends FragmentPagerAdapter {

    private Context context;
    private List<Fragment> listFrag;


    public DepartmentsAdapter(FragmentManager fm, List<Fragment> listFrag) {
        super(fm);
        this.listFrag=listFrag;
    }

    @Override
    public Fragment getItem(int position) {
        return listFrag.get(position);
    }

    @Override
    public int getCount() {
        return listFrag.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {

        if (position==0){
            return "About";
        }

        if (position==1){
            return "Faculties";
        }


        if (position==2){
            return "Academic Calendar";
        }

        if (position==3){
            return "Time Table";
        }
        return super.getPageTitle(position);
    }
}
