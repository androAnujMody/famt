package com.anuj.famt.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anuj.famt.R;
import com.anuj.famt.departments.DepartmentDetails;
import com.anuj.famt.models.DeptMainModel;
import com.anuj.famt.utils.Constants;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by anuj on 06/03/18.
 */

public class DeptMainAdapter extends RecyclerView.Adapter<DeptMainAdapter.DeptMainVH> {

    private Context context;
    private List<DeptMainModel> list;
    private LayoutInflater inflater;

    public DeptMainAdapter(Context context, List<DeptMainModel> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public DeptMainAdapter.DeptMainVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DeptMainVH(inflater.inflate(R.layout.single_department_item_new, parent, false));
    }

    @Override
    public void onBindViewHolder(DeptMainAdapter.DeptMainVH holder, int position) {

        Glide.with(context).load(list.get(position).deptImage).into(holder.ivImage);
        holder.tvName.setText(list.get(position).deptName);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class DeptMainVH extends RecyclerView.ViewHolder {
        private ImageView ivImage;
        private TextView tvName;

        public DeptMainVH(View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.iv_dept_image);
            tvName = itemView.findViewById(R.id.tv_dept);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DepartmentDetails.class);
                    if (getLayoutPosition() == 0) {
                        intent.putExtra(Constants.DEPT, "Mechanical");
                    }

                    if (getLayoutPosition() == 1) {
                        intent.putExtra(Constants.DEPT, "Chemical");
                    }

                    if (getLayoutPosition() == 2) {
                        intent.putExtra(Constants.DEPT, "Electrical");
                    }

                    if (getLayoutPosition() == 3) {
                        intent.putExtra(Constants.DEPT, "EXTC");
                    }

                    if (getLayoutPosition() == 4) {
                        intent.putExtra(Constants.DEPT, "IT");
                    }

                    if (getLayoutPosition() == 5) {
                        intent.putExtra(Constants.DEPT, "Electronic");
                    }

                    if (getLayoutPosition() == 6) {
                        intent.putExtra(Constants.DEPT, "MCA");
                    }

                    if (getLayoutPosition() == 7) {
                        intent.putExtra(Constants.DEPT, "Firstyear");
                    }

                    context.startActivity(intent);
                }
            });
        }
    }
}
