package com.anuj.famt.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anuj.famt.R;
import com.anuj.famt.models.Notice;

import java.util.List;

/**
 * Created by anuj on 09/03/18.
 */

public class NoticeAdapter extends RecyclerView.Adapter<NoticeAdapter.NoticeVH> {


    private Context context;
    private List<Notice> listData;
    private LayoutInflater inflater;


    public NoticeAdapter(Context context, List<Notice> listData) {
        this.context = context;
        this.listData = listData;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public NoticeAdapter.NoticeVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NoticeVH(inflater.inflate(R.layout.single_home_news, parent, false));
    }

    @Override
    public void onBindViewHolder(NoticeAdapter.NoticeVH holder, int position) {

        holder.tvDate.setText(listData.get(position).date);
        holder.tvTitle.setText(listData.get(position).heading);
        holder.tvNews.setText(listData.get(position).detail);

    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class NoticeVH extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView tvTitle;
        TextView tvDate;
        TextView tvNews;
        TextView tvReadMore;


        public NoticeVH(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView3);
            tvTitle = (TextView) itemView.findViewById(R.id.textView3);
            tvDate = (TextView) itemView.findViewById(R.id.textView4);
            tvNews = (TextView) itemView.findViewById(R.id.textView5);
            tvReadMore = itemView.findViewById(R.id.textView6);
            imageView.setVisibility(View.GONE);
            tvReadMore.setVisibility(View.GONE);

        }
    }
}
