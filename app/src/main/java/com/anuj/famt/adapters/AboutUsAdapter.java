package com.anuj.famt.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by gaurav on 26/10/17.
 */

public class AboutUsAdapter extends FragmentPagerAdapter {

    private Context context;
    private List<Fragment> listFrag;


    public AboutUsAdapter(FragmentManager fm,List<Fragment> listFrag) {
        super(fm);
        this.listFrag=listFrag;
    }

    @Override
    public Fragment getItem(int position) {
        return listFrag.get(position);
    }

    @Override
    public int getCount() {
        return listFrag.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {

        if (position==0){
            return "About Us";
        }

        if (position==1){
            return "Vision And Mission";
        }


        if (position==2){
            return "About Us";
        }
        return super.getPageTitle(position);
    }
}
