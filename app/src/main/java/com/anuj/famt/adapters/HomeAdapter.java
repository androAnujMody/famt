package com.anuj.famt.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anuj.famt.R;
import com.anuj.famt.home.Details;
import com.anuj.famt.models.HomeModel;
import com.anuj.famt.utils.Constants;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by gaurav on 26/10/17.
 */

public class HomeAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<HomeModel> listHome;
    private LayoutInflater inflater;
    private int IMAGE_HEADER = 1;
    private int HEADER = 2;
    private int NEWS = 3;
    private int EVENTS = 4;

    public HomeAdapter(Context context, List<HomeModel> listHome) {
        this.context = context;
        this.listHome = listHome;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getItemViewType(int position) {

        if (listHome.get(position).isHeaderImage) {
            return IMAGE_HEADER;
        } else if (listHome.get(position).isHeader) {
            return HEADER;
        } else if (listHome.get(position).isNews) {
            return NEWS;
        } else {
            return EVENTS;
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == IMAGE_HEADER) {
            return new ImageHeader(inflater.inflate(R.layout.single_home_header_image, parent, false));
        } else if (viewType == HEADER) {
            return new Header(inflater.inflate(R.layout.single_home_header, parent, false));
        } else if (viewType == NEWS) {
            return new News(inflater.inflate(R.layout.single_home_news, parent, false));
        } else {
            return new Events(inflater.inflate(R.layout.single_home_events, parent, false));
        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (listHome.get(position).isHeaderImage) {
            ImageHeader imageHeader = (ImageHeader) holder;
            Glide.with(context).load(R.drawable.famt_home_image).into(imageHeader.imageView);
        } else if (listHome.get(position).isHeader) {
            Header header = (Header) holder;
            header.tvHeader.setText(listHome.get(position).header);

        } else if (listHome.get(position).isNews) {
            News news = (News) holder;
            news.tvTitle.setText(listHome.get(position).heading);
            news.tvDate.setText(listHome.get(position).date);
            news.tvNews.setText(listHome.get(position).detail);
            if (!TextUtils.isEmpty(listHome.get(position).img)) {
                Glide.with(context).load(listHome.get(position).img).into(news.imageView);
            }
        } else {
            Events events = (Events) holder;
            events.tvTitle.setText(listHome.get(position).heading);
            events.tvDate.setText(listHome.get(position).date);

        }
    }

    @Override
    public int getItemCount() {
        return listHome.size();
    }

    public class ImageHeader extends RecyclerView.ViewHolder {

        ImageView imageView;

        public ImageHeader(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView2);
        }
    }

    public class Header extends RecyclerView.ViewHolder {
        TextView tvHeader;


        public Header(View itemView) {
            super(itemView);
            tvHeader = (TextView) itemView.findViewById(R.id.tv_header);

        }
    }

    public class News extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView tvTitle;
        TextView tvDate;
        TextView tvNews;

        public News(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView3);
            tvTitle = (TextView) itemView.findViewById(R.id.textView3);
            tvDate = (TextView) itemView.findViewById(R.id.textView4);
            tvNews = (TextView) itemView.findViewById(R.id.textView5);
            tvNews.setMaxLines(3);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, Details.class);
                    intent.putExtra(Constants.HEADER, listHome.get(getLayoutPosition()).heading);
                    intent.putExtra(Constants.IMAGE, listHome.get(getLayoutPosition()).img);
                    intent.putExtra(Constants.DETAILS, listHome.get(getLayoutPosition()).detail);
                    context.startActivity(intent);

                }
            });
        }
    }

    public class Events extends RecyclerView.ViewHolder {

        TextView tvTitle;
        TextView tvDate;

        public Events(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.textView7);
            tvDate = (TextView) itemView.findViewById(R.id.textView4);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, Details.class);
                    intent.putExtra(Constants.HEADER, listHome.get(getLayoutPosition()).heading);
                    intent.putExtra(Constants.DETAILS, listHome.get(getLayoutPosition()).detail);
                    context.startActivity(intent);

                }
            });
        }
    }
}
