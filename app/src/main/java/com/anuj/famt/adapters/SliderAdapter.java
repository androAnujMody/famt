package com.anuj.famt.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.anuj.famt.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

/**
 * Created by playtm on 24/03/18.
 */

public class SliderAdapter extends PagerAdapter {

    private final RequestOptions requestOptions;
    private Context context;
    private List<String> list;
    private LayoutInflater inflater;

    public SliderAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
         requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_image_black_24dp);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View item = inflater.inflate(R.layout.custom_pager_item, container, false);
        ImageView imageView = (ImageView) item.findViewById(R.id.iv);
        //Ion.with(imageView).load("https://pharmore.in" + list.get(position).imageUrl);

        Glide.with(context).load(list.get(position)).apply(requestOptions).into(imageView);
        container.addView(item);

        return item;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
