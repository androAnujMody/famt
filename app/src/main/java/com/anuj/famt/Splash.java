package com.anuj.famt;

import android.os.Bundle;
import android.os.Handler;

import com.anuj.famt.home.HomeActivity;

public class Splash extends BaseActivity {

    @Override
    protected int getParentView() {
        return R.layout.activity_splash;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivty(HomeActivity.class);
                finish();
            }
        }, 2000);
    }
}
