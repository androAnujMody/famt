package com.anuj.famt.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by anuj on 27/10/17.
 */

public class SingleModel implements Parcelable {
    public String name;
    public String link;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.link);
    }

    public SingleModel() {
    }

    protected SingleModel(Parcel in) {
        this.name = in.readString();
        this.link = in.readString();
    }

    public static final Parcelable.Creator<SingleModel> CREATOR = new Parcelable.Creator<SingleModel>() {
        @Override
        public SingleModel createFromParcel(Parcel source) {
            return new SingleModel(source);
        }

        @Override
        public SingleModel[] newArray(int size) {
            return new SingleModel[size];
        }
    };
}
