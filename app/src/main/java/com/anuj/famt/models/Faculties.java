package com.anuj.famt.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by playtm on 17/02/18.
 */
public class Faculties implements Parcelable {
    public String designation;
    public String facultyImg;
    public String facultyName;
    public String qualification;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.designation);
        dest.writeString(this.facultyImg);
        dest.writeString(this.facultyName);
        dest.writeString(this.qualification);
    }

    public Faculties() {
    }

    protected Faculties(Parcel in) {
        this.designation = in.readString();
        this.facultyImg = in.readString();
        this.facultyName = in.readString();
        this.qualification = in.readString();
    }

    public static final Parcelable.Creator<Faculties> CREATOR = new Parcelable.Creator<Faculties>() {
        @Override
        public Faculties createFromParcel(Parcel source) {
            return new Faculties(source);
        }

        @Override
        public Faculties[] newArray(int size) {
            return new Faculties[size];
        }
    };
}
