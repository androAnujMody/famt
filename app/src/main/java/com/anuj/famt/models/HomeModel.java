package com.anuj.famt.models;

/**
 * Created by gaurav on 26/10/17.
 */

public class HomeModel {
    public boolean isHeaderImage;
    public boolean isHeader;
    public boolean isNews;
    public boolean isEvents;
    public String header;
    public String heading;
    public String date;
    public String detail;
    public String img;
}
