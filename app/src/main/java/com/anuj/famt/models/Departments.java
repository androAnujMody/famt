package com.anuj.famt.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by playtm on 12/02/18.
 */

public class Departments implements Parcelable {
    public String deptAbout;
    public List<SingleModel> timetable = new ArrayList<>();
    public List<Faculties> Faculties = new ArrayList<>();
    public String academicCalender;
    public String deptImg;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.deptAbout);
        dest.writeTypedList(this.timetable);
        dest.writeTypedList(this.Faculties);
        dest.writeString(this.academicCalender);
        dest.writeString(this.deptImg);
    }

    public Departments() {
    }

    protected Departments(Parcel in) {
        this.deptAbout = in.readString();
        this.timetable = in.createTypedArrayList(SingleModel.CREATOR);
        this.Faculties = in.createTypedArrayList(com.anuj.famt.models.Faculties.CREATOR);
        this.academicCalender = in.readString();
        this.deptImg = in.readString();
    }

    public static final Creator<Departments> CREATOR = new Creator<Departments>() {
        @Override
        public Departments createFromParcel(Parcel source) {
            return new Departments(source);
        }

        @Override
        public Departments[] newArray(int size) {
            return new Departments[size];
        }
    };
}

class timeTable implements Parcelable {
    public String name;
    public String link;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.link);
    }

    public timeTable() {
    }

    protected timeTable(Parcel in) {
        this.name = in.readString();
        this.link = in.readString();
    }

    public static final Parcelable.Creator<timeTable> CREATOR = new Parcelable.Creator<timeTable>() {
        @Override
        public timeTable createFromParcel(Parcel source) {
            return new timeTable(source);
        }

        @Override
        public timeTable[] newArray(int size) {
            return new timeTable[size];
        }
    };
}






