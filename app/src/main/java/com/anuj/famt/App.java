package com.anuj.famt;

import android.app.Application;
import android.content.Intent;
import android.widget.Toast;

import com.anuj.famt.utils.Constants;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

/**
 * Created by anuj on 24/03/18.
 */

public class App extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        //test();
        startService(new Intent(this, MyService.class));
    }

    public void test() {

        Constants.FIREBASE_REF.child(Constants.EVENTS).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Toast.makeText(App.this, "added", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
