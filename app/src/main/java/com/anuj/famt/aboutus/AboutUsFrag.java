package com.anuj.famt.aboutus;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anuj.famt.BaseFragment;
import com.anuj.famt.R;

/**
 * Created by gaurav on 26/10/17.
 */

public class AboutUsFrag extends BaseFragment {



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_about_us, container, false);
        return view;
    }
}
