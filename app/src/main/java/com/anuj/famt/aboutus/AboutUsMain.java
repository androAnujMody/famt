package com.anuj.famt.aboutus;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;

import com.anuj.famt.BaseActivity;
import com.anuj.famt.R;
import com.anuj.famt.adapters.AboutUsAdapter;
import com.anuj.famt.utils.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anuj on 25/10/17.
 */

public class AboutUsMain extends BaseActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private AboutUsAdapter aboutUsAdapter;

    @Override
    protected int getParentView() {
        return R.layout.activity_about_us_main;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTitle("About Us");
        findViewByIds();
    }

    private void findViewByIds() {
        Constants.FROM_ABOUT = true;
        tabLayout = (TabLayout) findViewById(R.id.tab);
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        setUpViewPager();
        setUpTabs();


    }

    private void setUpTabs() {


        TabLayout.Tab home = tabLayout.newTab();
        home.setText("About Us");
        tabLayout.addTab(home);

        TabLayout.Tab visionAndMission = tabLayout.newTab();
        visionAndMission.setText("Vision And Mission");
        tabLayout.addTab(visionAndMission);

        TabLayout.Tab staff = tabLayout.newTab();
        staff.setText("Staff");
        tabLayout.addTab(staff);

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(ContextCompat.getColorStateList(this, R.color.white));


    }

    private void setUpViewPager() {
        aboutUsAdapter = new AboutUsAdapter(getSupportFragmentManager(), getFrags());
        viewPager.setAdapter(aboutUsAdapter);
    }

    private List<Fragment> getFrags() {

        List<Fragment> list = new ArrayList<>(2);
        list.add(new AboutUsFrag());
        list.add(new VisionAndMissionFrag());
        return list;
    }
}
