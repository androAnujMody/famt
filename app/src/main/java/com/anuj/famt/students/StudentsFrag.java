package com.anuj.famt.students;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.anuj.famt.BaseFragment;
import com.anuj.famt.R;
import com.anuj.famt.models.SingleModel;
import com.anuj.famt.notices.NoticeActivity;
import com.anuj.famt.placements.PlacementActivity;
import com.anuj.famt.utils.WebViewActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anuj on 27/10/17.
 */

public class StudentsFrag extends BaseFragment {
    private RecyclerView rvDepartments;
    private List<SingleModel> list;
    private Button btNotice, btResults, btExam;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_students, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        findViewByIds();
    }

    private void findViewByIds() {
        init(false);
        setToolbarTitle("Students");
        btNotice = view.findViewById(R.id.bt_notice);
        btExam = view.findViewById(R.id.bt_exam);
        btResults = view.findViewById(R.id.bt_results);
        /*rvDepartments = (RecyclerView) view.findViewById(R.id.rv_home);
        rvDepartments.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvDepartments.setAdapter(new SingleItemAdapter(getActivity(), getList(), Constants.STUDENT));*/

        btNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), NoticeActivity.class);
                startActivity(intent);
            }
        });

        btExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PlacementActivity.class);
                intent.putExtra("title", "Exam");
                startActivity(intent);
            }
        });

        btResults.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), WebViewActivity.class);
                intent.putExtra("link", "https://drive.google.com/embeddedfolderview?id=185GcKo-TiJHmHrHKEvSgci2TmGz8oWo3#grid");
                intent.putExtra("title", "Results");
                startActivity(intent);
            }
        });
    }

    private List<SingleModel> getList() {

        list = new ArrayList<>();
        SingleModel model = new SingleModel();
        model.name = "STUDENT NOTICES";
        list.add(model);

        model = new SingleModel();
        model.name = "EXAMINATION";
        list.add(model);

        model = new SingleModel();
        model.name = "RESULTS";
        list.add(model);


        return list;
    }
}
