package com.anuj.famt.utils;

import com.anuj.famt.models.HomeModel;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

/**
 * Created by anuj on 07/01/18.
 */

public class Constants {

    public static final DatabaseReference FIREBASE_REF = FirebaseDatabase.getInstance().getReference();
    public static final String NEWS = "news";
    public static final String EVENTS = "events";
    public static final String HEADER = "header";
    public static final String IMAGE = "image";
    public static final String DETAILS = "details";
    public static final String DEPT = "Dept";
    public static final String STUDENT = "Student";
    public static final String DEPARTMENTS = "departments";
    public static final String TT = "TimeTable";
    public static final String NOTICES = "notices";
    public static final String PLACEMENT_IMAGES = "scrollImages";
    public static final String PLACEMENT = "placements";
    public static boolean FROM_ABOUT = false;
    public static boolean SHOW_NOTI = false;
    public static List<HomeModel> HOME_LIST ;
}
