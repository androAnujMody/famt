package com.anuj.famt;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;


/**
 * Created by anuj on 30/09/17.
 */

public abstract class BaseActivity extends AppCompatActivity {

    protected Toolbar toolbar;
    protected int height;
    protected int width;

    protected abstract int getParentView();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getParentView());
        init();
    }

    protected void setToolbarTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        getScreenDimens();
    }

    protected void showAlert(String header, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(header).
                setMessage(msg).
                setCancelable(true).
                setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
    }

    protected void startActivty(Class className) {
        startActivity(new Intent(this, className));
    }

    protected Map<String, List<String>> generateParams(String[] key, String[] values) {
        Map<String, List<String>> map = new WeakHashMap<>();
        List<String> listValues;

        for (int i = 0; i < key.length; i++) {
            listValues = new ArrayList<>(1);
            listValues.add(values[i]);
            map.put(key[i], listValues);
        }


        return map;
    }

    public static void replaceFragment(FragmentActivity activity, int fragId, Fragment fragment, boolean addToBackStack) {


        try {
            String backStateName = fragment.getClass().getName();
            String fragmentTag = backStateName;
            FragmentManager manager = activity.getSupportFragmentManager();
            boolean fragmentPopped = manager.popBackStackImmediate(fragmentTag, 0);
            if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) {
                FragmentTransaction ft = manager.beginTransaction();

                ft.replace(fragId, fragment, fragmentTag);
                if (addToBackStack) {
                    ft.addToBackStack(backStateName);
                }
                ft.commit();
            }
        } catch (Exception e) {

            e.printStackTrace();
        }


    }

    protected void getScreenDimens() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
    }



}
