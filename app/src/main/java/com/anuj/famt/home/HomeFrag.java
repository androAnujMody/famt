package com.anuj.famt.home;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.anuj.famt.BaseFragment;
import com.anuj.famt.R;
import com.anuj.famt.adapters.HomeAdapter;
import com.anuj.famt.models.HomeModel;
import com.anuj.famt.utils.CommonFunctions;
import com.anuj.famt.utils.Constants;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gaurav on 26/10/17.
 */

public class HomeFrag extends BaseFragment {

    private RecyclerView rvHome;
    private List<HomeModel> listHome;
    private ProgressBar progressBar;
    private LinearLayout noInternet;
    private HomeAdapter homeAdapter;
    SharedPreferences pref;
    SharedPreferences.Editor editor;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_home, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        findViewByIds();
    }

    private void findViewByIds() {
        init(false);
        pref = getActivity().getSharedPreferences("FAMT", 0); // 0 - for private mode
        editor = pref.edit();
        editor.putBoolean("added", false).apply();
        hideToolbar();
        listHome = new ArrayList<>();
        progressBar = view.findViewById(R.id.progress);
        progressBar.setVisibility(View.VISIBLE);
        noInternet = view.findViewById(R.id.ll_ni);
        rvHome = (RecyclerView) view.findViewById(R.id.rv_home);
        rvHome.setLayoutManager(new LinearLayoutManager(getActivity()));

        if (Constants.HOME_LIST == null) {
            if (CommonFunctions.isConnected(getActivity())) {
                getNewsData();
            } else {
                noInternet.setVisibility(View.VISIBLE);
            }
        } else {
            homeAdapter = new HomeAdapter(getActivity(), Constants.HOME_LIST);
            rvHome.setAdapter(homeAdapter);
            progressBar.setVisibility(View.GONE);
        }

        //test();


    }

    private void getNewsData() {
        Constants.FIREBASE_REF.child(Constants.NEWS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot != null) {
                    HomeModel model = new HomeModel();
                    model.isHeaderImage = true;
                    listHome.add(model);

                    model = new HomeModel();
                    model.isHeader = true;
                    model.header = "News";
                    listHome.add(model);

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        listHome.add(snapshot.getValue(HomeModel.class));
                    }

                    getEvents();


                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                noInternet.setVisibility(View.VISIBLE);
            }
        });
    }

    private void getEvents() {

        Constants.FIREBASE_REF.child(Constants.EVENTS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot != null) {
                    HomeModel model;

                    model = new HomeModel();
                    model.isHeader = true;
                    model.header = "Events";
                    listHome.add(model);

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        listHome.add(snapshot.getValue(HomeModel.class));
                    }

                    progressBar.setVisibility(View.GONE);
                    if (isVisible()) {
                        Constants.HOME_LIST = listHome;
                        homeAdapter = new HomeAdapter(getActivity(), listHome);
                        rvHome.setAdapter(homeAdapter);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                editor.putBoolean("added", true).apply();
                            }
                        }, 5000);

                    }


                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                noInternet.setVisibility(View.VISIBLE);
            }
        });
    }

    public void test() {

        Constants.FIREBASE_REF.child(Constants.EVENTS).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    /*private List<HomeModel> getHomeList() {

        List<HomeModel> list = new ArrayList<>();
        HomeModel model = new HomeModel();
        model.isHeaderImage = true;
        list.add(model);

        model = new HomeModel();
        model.isHeader = true;
        model.header = "News";
        list.add(model);

        model = new HomeModel();
        model.isNews = true;
        model.title = "News 1";
        model.date = "28/10/2017";
        model.news = "Dummy news 1";
        list.add(model);

        model = new HomeModel();
        model.isNews = true;
        model.title = "News 2";
        model.date = "29/10/2017";
        model.news = "Dummy news 1";
        list.add(model);

        model = new HomeModel();
        model.isNews = true;
        model.title = "News 3";
        model.date = "30/10/2017";
        model.news = "Dummy news 1";
        list.add(model);

        model = new HomeModel();
        model.isHeader = true;
        model.header = "Events";
        list.add(model);

        model = new HomeModel();
        model.isEvents = true;
        model.title = "Events 1";
        model.date = "30/10/2017";
        model.news = "Events 1";
        list.add(model);

        model = new HomeModel();
        model.isEvents = true;
        model.title = "Events 2";
        model.date = "31/10/2017";
        model.news = "Events 1";
        list.add(model);
        return list;
    }*/
}
