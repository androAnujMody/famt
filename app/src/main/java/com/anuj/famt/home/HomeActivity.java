package com.anuj.famt.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.view.MenuItem;

import com.anuj.famt.BaseActivity;
import com.anuj.famt.R;
import com.anuj.famt.aboutus.AboutUsMain;
import com.anuj.famt.departments.DepartmentsFrag;
import com.anuj.famt.students.StudentsFrag;
import com.anuj.famt.utils.Constants;

/**
 * Created by anuj on 25/10/17.
 */

public class HomeActivity extends BaseActivity {

    private BottomNavigationView bottomNavigationMenu;

    @Override
    protected int getParentView() {
        return R.layout.activity_home;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        replaceFragment(this, R.id.frag_container, new HomeFrag(), false);
        findViewByIds();
    }

    private void findViewByIds() {
        bottomNavigationMenu = (BottomNavigationView) findViewById(R.id.tabs);
        bottomNavigationMenu.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_home:
                        openFrag(1);
                        break;

                    case R.id.action_dept:
                        openFrag(2);
                        break;

                    case R.id.action_students:
                        openFrag(3);
                        break;

                    case R.id.action_about:
                        openFrag(4);
                        break;
                }

                return true;
            }
        });
    }

    private void openFrag(int i) {
        switch (i) {
            case 1:
                replaceFragment(this, R.id.frag_container, new HomeFrag(), false);
                break;

            case 2:
                replaceFragment(this, R.id.frag_container, new DepartmentsFrag(), false);
                break;

            case 3:
                replaceFragment(this, R.id.frag_container, new StudentsFrag(), false);
                break;

            case 4:
                startActivty(AboutUsMain.class);
                break;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (Constants.FROM_ABOUT) {
            Constants.FROM_ABOUT = false;
            bottomNavigationMenu.setSelectedItemId(R.id.action_home);
        }


    }
}
