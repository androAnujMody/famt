package com.anuj.famt.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.anuj.famt.BaseActivity;
import com.anuj.famt.R;
import com.anuj.famt.utils.Constants;
import com.bumptech.glide.Glide;

/**
 * Created by anuj on 07/01/18.
 */

public class Details extends BaseActivity {

    private ImageView ivImage;
    private TextView tvHeader;
    private TextView tvDetails;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUp();

    }

    private void setUp() {

        ivImage = findViewById(R.id.iv_image);
        tvHeader = findViewById(R.id.tv_header);
        tvDetails = findViewById(R.id.tv_details);




        if (!TextUtils.isEmpty(getIntent().getStringExtra(Constants.HEADER))) {
            tvHeader.setText(getIntent().getStringExtra(Constants.HEADER));
            setToolbarTitle(getIntent().getStringExtra(Constants.HEADER));
        }

        if (!TextUtils.isEmpty(getIntent().getStringExtra(Constants.DETAILS))) {
            tvDetails.setText(getIntent().getStringExtra(Constants.DETAILS));
        }

        if (!TextUtils.isEmpty(getIntent().getStringExtra(Constants.IMAGE))) {
            Glide.with(this).load(getIntent().getStringExtra(Constants.IMAGE)).into(ivImage);
        }


    }

    @Override
    protected int getParentView() {
        return R.layout.news_events_details;
    }
}
