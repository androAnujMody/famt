package com.anuj.famt.notices;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.anuj.famt.BaseActivity;
import com.anuj.famt.R;
import com.anuj.famt.adapters.NoticeAdapter;
import com.anuj.famt.models.Notice;
import com.anuj.famt.utils.Constants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anuj on 09/03/18.
 */

public class NoticeActivity extends BaseActivity {

    private RecyclerView rvNotice;
    private List<Notice> listNotice;
    private ProgressBar progressBar;
    private LinearLayout noInternet;
    private NoticeAdapter adapter;

    @Override
    protected int getParentView() {
        return R.layout.frag_home;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUp();
    }

    private void setUp() {
        setToolbarTitle("Notices");
        listNotice = new ArrayList<>();
        progressBar = findViewById(R.id.progress);
        progressBar.setVisibility(View.VISIBLE);
        noInternet = findViewById(R.id.ll_ni);
        rvNotice = (RecyclerView) findViewById(R.id.rv_home);
        rvNotice.setLayoutManager(new LinearLayoutManager(this));
        callWS();
    }

    private void callWS() {
        Constants.FIREBASE_REF.child(Constants.NOTICES).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot != null) {

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        listNotice.add(snapshot.getValue(Notice.class));
                    }
                    rvNotice.setAdapter(new NoticeAdapter(NoticeActivity.this, listNotice));
                    progressBar.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                noInternet.setVisibility(View.VISIBLE);
            }
        });
    }
}
