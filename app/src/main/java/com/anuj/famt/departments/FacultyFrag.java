package com.anuj.famt.departments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anuj.famt.BaseFragment;
import com.anuj.famt.R;
import com.anuj.famt.adapters.FacultyAdapter;
import com.anuj.famt.models.Departments;

/**
 * Created by playtm on 17/02/18.
 */


public class FacultyFrag extends BaseFragment {

    private Departments model;
    private static final String ARG1 = "model";
    private RecyclerView rvFaculties;
    private Toolbar toolbar;

    public static FacultyFrag newInstance(Departments model) {
        FacultyFrag fragment = new FacultyFrag();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG1, model);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_home, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {

        model = (Departments) getArguments().get(ARG1);
        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
        rvFaculties = view.findViewById(R.id.rv_home);
        rvFaculties.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvFaculties.setAdapter(new FacultyAdapter(getActivity(), model.Faculties));

    }
}
