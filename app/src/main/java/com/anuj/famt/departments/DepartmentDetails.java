package com.anuj.famt.departments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.WindowManager;

import com.anuj.famt.BaseActivity;
import com.anuj.famt.R;
import com.anuj.famt.adapters.DepartmentsAdapter;
import com.anuj.famt.models.Departments;
import com.anuj.famt.utils.Constants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anuj on 13/01/18.
 */

public class DepartmentDetails extends BaseActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private List<Departments> listDept;
    private DepartmentsAdapter adapter;


    @Override
    protected int getParentView() {
        return R.layout.activity_dept_details_main;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTitle(getIntent().getStringExtra(Constants.DEPT));
        findViewByIds();
    }

    private void findViewByIds() {

        tabLayout = (TabLayout) findViewById(R.id.tab);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setUpTabs();
        callWS();

    }

    private void callWS() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        Constants.FIREBASE_REF.child(Constants.DEPARTMENTS).child(getIntent().getStringExtra(Constants.DEPT)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (listDept == null) {
                    listDept = new ArrayList<>();
                }
                listDept.add(dataSnapshot.getValue(Departments.class));
                setUpViewPager();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setUpViewPager() {

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        adapter = new DepartmentsAdapter(getSupportFragmentManager(), getFragList());
        viewPager.setAdapter(adapter);

    }

    private List<Fragment> getFragList() {

        List<Fragment> listFrags = new ArrayList<>();
        listFrags.add(DeptAbtFrag.newInstance(listDept.get(0)));
        listFrags.add(FacultyFrag.newInstance(listDept.get(0)));
        listFrags.add(AcademicCalFrag.newInstance(listDept.get(0)));
        listFrags.add(TimeTableFrag.newInstance(listDept.get(0)));
        return listFrags;
    }

    private void setUpTabs() {


        TabLayout.Tab home = tabLayout.newTab();
        //home.setText("Home");
        tabLayout.addTab(home);

        TabLayout.Tab faculties = tabLayout.newTab();
        //faculties.setText("Faculties");
        tabLayout.addTab(faculties);

        TabLayout.Tab calender = tabLayout.newTab();
        //calender.setText("Academic Calender");
        tabLayout.addTab(calender);

        TabLayout.Tab tt = tabLayout.newTab();
        //tt.setText("Time Table");
        tabLayout.addTab(tt);

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(ContextCompat.getColorStateList(this, R.color.white));


    }
}
