package com.anuj.famt.departments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anuj.famt.BaseFragment;
import com.anuj.famt.R;
import com.anuj.famt.adapters.DeptMainAdapter;
import com.anuj.famt.models.DeptMainModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anuj on 27/10/17.
 */

public class DepartmentsFrag extends BaseFragment {

    private RecyclerView rvDepartments;
    private List<DeptMainModel> list;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_home, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        findViewByIds();
    }

    private void findViewByIds() {
        init(false);
        setToolbarTitle("Departments");

        rvDepartments = (RecyclerView) view.findViewById(R.id.rv_home);
        rvDepartments.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvDepartments.setAdapter(new DeptMainAdapter(getActivity(),getList()));
    }

    private List<DeptMainModel> getList() {
        list = new ArrayList<>();
        DeptMainModel model = new DeptMainModel();
        model.deptName = "MECHANICAL ENGINEERING";
        model.deptImage = R.drawable.mechanical;
        list.add(model);

        model = new DeptMainModel();
        model.deptName = "CHEMICAL ENGINEERING";
        model.deptImage = R.drawable.chemical;
        list.add(model);

        model = new DeptMainModel();
        model.deptName = "ELECTRICAL ENGINEERING";
        model.deptImage = R.drawable.electrical;
        list.add(model);

        model = new DeptMainModel();
        model.deptName = "EXTC ENGINEERING";
        model.deptImage = R.drawable.extc;
        list.add(model);

        model = new DeptMainModel();
        model.deptName = "IT ENGINEERING";
        model.deptImage = R.drawable.it;
        list.add(model);

        model = new DeptMainModel();
        model.deptName = "ELECTRONICS ENGINEERING";
        model.deptImage = R.drawable.electronic;
        list.add(model);
        model = new DeptMainModel();
        model.deptName = "MCA";
        model.deptImage = R.drawable.mca;
        list.add(model);

        model = new DeptMainModel();
        model.deptName = "FIRST YEAR ENGINEERING";
        model.deptImage = R.drawable.fe;
        list.add(model);

        return list;
    }

    /*private List<SingleModel> getList() {

        list = new ArrayList<>();
        SingleModel model = new SingleModel();
        model.name = "MECHANICAL ENGINEERING";
        list.add(model);

        model = new SingleModel();
        model.name = "CHEMICAL ENGINEERING";
        list.add(model);

        model = new SingleModel();
        model.name = "ELECTRICAL ENGINEERING";
        list.add(model);

        model = new SingleModel();
        model.name = "EXTC ENGINEERING";
        list.add(model);

        model = new SingleModel();
        model.name = "IT ENGINEERING";
        list.add(model);

        model = new SingleModel();
        model.name = "ELECTRONICS ENGINEERING";
        list.add(model);

        model = new SingleModel();
        model.name = "MCA";
        list.add(model);

        return list;
    }*/
}
