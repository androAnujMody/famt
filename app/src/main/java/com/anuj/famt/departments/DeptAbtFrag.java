package com.anuj.famt.departments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.anuj.famt.BaseFragment;
import com.anuj.famt.R;
import com.anuj.famt.models.Departments;
import com.bumptech.glide.Glide;

/**
 * Created by playtm on 12/02/18.
 */

public class DeptAbtFrag extends BaseFragment {

    private ImageView imageView;
    private TextView tvData;
    private Departments model;
    private ProgressBar progressBar;

    private static final String ARG1 = "model";

    public static DeptAbtFrag newInstance(Departments model) {
        DeptAbtFrag fragment = new DeptAbtFrag();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG1, model);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_about_us, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {
        imageView = view.findViewById(R.id.imageView);
        tvData = view.findViewById(R.id.tv_data);
        progressBar = view.findViewById(R.id.progress);
        progressBar.setVisibility(View.VISIBLE);
        model = (Departments) getArguments().get(ARG1);
        tvData.setText(model.deptAbout);
        tvData.setText(Html.fromHtml(model.deptAbout));
        progressBar.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(model.deptImg)) {
            Glide.with(this).load(model.deptImg).into(imageView);
        }
    }
}
