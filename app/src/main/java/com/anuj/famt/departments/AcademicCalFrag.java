package com.anuj.famt.departments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.anuj.famt.BaseFragment;
import com.anuj.famt.R;
import com.anuj.famt.models.Departments;

/**
 * Created by playtm on 17/02/18.
 */

public class AcademicCalFrag extends BaseFragment {

    private Departments model;
    private static final String ARG1 = "model";
    private WebView webView;
    private Toolbar toolbar;
    private ProgressBar progressBar;

    public static AcademicCalFrag newInstance(Departments model) {
        AcademicCalFrag fragment = new AcademicCalFrag();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG1, model);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.webview, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {

        progressBar=view.findViewById(R.id.progress);
        model = (Departments) getArguments().get(ARG1);
        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
        webView = view.findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.GONE);
            }
        });
        webView.loadUrl("https://docs.google.com/viewer?url=" + model.academicCalender);

    }
}
